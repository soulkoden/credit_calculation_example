const {CREDIT_PRODUCT_NAME} = require('./config');

const {getProductInitialState} = require('./lib');

(async () => {
  const initialStateObject = await getProductInitialState({
    creditProduct: CREDIT_PRODUCT_NAME,
  });

  console.log(JSON.stringify(initialStateObject, null, 2));
  /*
  {
    "data": {
      "creditProduct": {
        "initialAmount": 6000,
        "initialTerm": 20,
        "initialRepayment": 1809.35,
        "limits": [
          {
            "category": "amount",
            "min": 2000,
            "max": 20000,
            "step": 50   // 2000, 2050, 3000, 3050, ..., 19950, 20000
          },
          {
            "category": "term",
            "min": 12,
            "max": 72,
            "step": 1   // 12, 13, 14, 15, ..., 71, 72
          }
        ]
      }
    }
  }
  */
})();

// vim:ts=4:sw=2:
