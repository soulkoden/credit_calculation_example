const {
  CREDIT_PRODUCT_NAME,
  WORKFLOW_NAME,
} = require('./config');

const {
  constants,
  createAnonymousUserToken,
  getAvailableStepsByWorkflowToken,
  getCacheableObject,
  startWorkflowByName,
  submitWorkflowStep,
} = require('./lib');

// Start on the browser
(async () => {
  // First, we are need to have a user token
  // It's valid the 30 days
  const tokenObject = await getCacheableObject({
    key: constants.USER_TOKEN,
    objectCreationCallback: createAnonymousUserToken,
  });

  // Next, we are need to start our scenario for calculation
  // If your already started one does not need to start again
  const workflowObject = await getCacheableObject({
    key: constants.WORKFLOW_TOKEN,
    objectCreationCallback: () => startWorkflowByName({
      userToken: tokenObject.token,
      workflowName: WORKFLOW_NAME,
    }),
  });

  // Also we are need to choose a step from
  const availableSteps = await getCacheableObject({
    key: constants.AVAILABLE_STEPS,
    objectCreationCallback: () => getAvailableStepsByWorkflowToken({
      userToken: tokenObject.token,
      workflowToken: workflowObject.id,
    }),
  });

  const firstStep = availableSteps[0];

  // And finally, we are ready to final iterable calculations
  const calculatorData = {
    DESIRED_PRODUCT: CREDIT_PRODUCT_NAME,
    CAR_PRICE_IN_USD: '10000',
    DESIRED_TERM: 12,
    INITIAL_FEE_IN_USD: '2000',
  };

  const response1 = await submitWorkflowStep({
    userToken: tokenObject.token,
    workflowToken: workflowObject.id,
    stepToken: firstStep.id,
    sendingData: calculatorData,
  });

  console.log(JSON.stringify(response1, null, 2));
  /*
  {
    "amount": {
      "USD": "8000.07592",
      "UAH": "198117.88014"
    },
    "payment": {
      "USD": "2506",
      "UAH": "62060.42595"
    }
  }
  */

  // Now, user was change the some parameter, eg slider
  // And need to submit next time
  const response2 = await submitWorkflowStep({
    userToken: tokenObject.token,
    workflowToken: workflowObject.id,
    stepToken: firstStep.id,
    sendingData: {
      ...calculatorData,
      DESIRED_TERM: 24,
    },
  });

  console.log(JSON.stringify(response2, null, 2));
  /*
  {
    "amount": ...,
    "payment": {
      "USD": "2403",         // << See changes here
      "UAH": "59509.65824"   // << See changes here
    }
  }
  */

  // And repeat last step any times =)
})();

// vim:ts=4:sw=2:
