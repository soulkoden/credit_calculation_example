const fetch = require('node-fetch');

const {HOST} = require('./config');

const constants = {
  AVAILABLE_STEPS: 'AVAILABLE_STEPS',
  USER_TOKEN: 'USER_TOKEN',
  WORKFLOW_TOKEN: 'WORKFLOW_TOKEN',
};

// API methods
const createAnonymousUserToken = () => new Promise(async (resolve, reject) => {
  const url = `${HOST}/api/user/empty`;

  const response = await fetch(url, {
    method: 'POST',
  });

  resolve(await response.json());
});

const startWorkflowByName = ({
                               userToken,
                               workflowName,
                             }) => new Promise(async (resolve, reject) => {
  const url = `${HOST}/api/v1/workflow/${workflowName}/start`;

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${userToken}`,
    },
  });

  resolve(await response.json());
});

const getAvailableStepsByWorkflowToken = ({
                                            userToken,
                                            workflowToken,
                                          }) => new Promise(
    async (resolve, reject) => {
      const url = `${HOST}/api/v1/workflow/entrypoint/${workflowToken}/steps/next`;

      const response = await fetch(url, {
        headers: {
          'Authorization': `Bearer ${userToken}`,
        },
      });

      resolve(await response.json());
    });

const submitWorkflowStep = ({
                              userToken,
                              workflowToken,
                              stepToken,
                              sendingData,
                            }) => new Promise(async (resolve, reject) => {
  const url = `${HOST}/api/v1/workflow/entrypoint/${workflowToken}/step/${
      stepToken}/submit`;

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${userToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(sendingData),
  });

  resolve(await response.json());
});

const getProductInitialState = ({
                                  creditProduct,
                                }) => new Promise(async (resolve, reject) => {
  const response = await fetch(`${HOST}/graphql/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `
        query GetInitialState($creditProduct: String!) {
          creditProduct(name: $creditProduct) {
            initialAmount
            initialTerm
            initialRepayment
            limits {
              category
              min
              max
              step
            }
          }
        }
      `,
      variables: {
        creditProduct,
      },
    }),
  });

  resolve(await response.json());
});

// Fake storage and util
const localStorage = {
  getItem: (key) => this[key],
  hasItem: (key) => !!this[key],
  setItem: (key, value) => this[key] = value,
};

const getCacheableObject = ({
                              key,
                              objectCreationCallback,
                            }) => new Promise(async (resolve, reject) => {
  if (localStorage.hasItem(key)) {
    return resolve(localStorage.getItem(key));
  }

  const value = await objectCreationCallback();

  resolve(value);
  localStorage.setItem(key, value);
});

module.exports = {
  constants,
  createAnonymousUserToken,
  getAvailableStepsByWorkflowToken,
  getCacheableObject,
  getProductInitialState,
  localStorage,
  startWorkflowByName,
  submitWorkflowStep,
};

// vim:ts=4:sw=2:
