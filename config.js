const HOST = 'http://localhost:8000';
const WORKFLOW_NAME = 'ual_calculation_in_usd';
const CREDIT_PRODUCT_NAME = 'uah_annuity_09_2019';

module.exports = {
  CREDIT_PRODUCT_NAME,
  HOST,
  WORKFLOW_NAME,
};

// vim:ts=4:sw=2:
